#!/bin/bash
# wraps around commands to produce GPU kernel
# $1 = input ll name
# $2 = OpenCL/SPIR/HSAIL kernel file name
# $3 = (optional) --hsa will trigger HSAIL lowering
#      (optional) --spir will trigger SPIR lowering
#      (optional) --opencl will trigger OpenCL C lowering
#
# in case $3 is not set, then either SPIR or OpenCL C kernel
# will be emitted according to the capability of GPU

# enable bash debugging
KMDBSCRIPT="${KMDBSCRIPT:=0}"

# dump the LLVM bitcode
KMDUMPLLVM="${KMDUMPLLVM:=0}"

if [ $KMDBSCRIPT == "1" ]
then
  set -x
fi

# check command line arguments
if [ "$#" -lt 2 ]; then
  echo "Usage: $0 input_LLVM output_kernel (--hsa | --spir | --opencl) (--verbose)" >&2
  echo "  --hsa    will trigger HSAIL/BRIG kernel output" >&2
  echo "  --spir   will trigger SPIR       kernel output" >&2
  echo "  --opencl will trigger OpenCL C   kernel output" >&2
  echo "  --verbose will enable GPU target lowering output" >&2
  exit 1
fi

if [ ! -f $1 ]; then
  echo "input LLVM IR $1 is not valid" >&2
  exit 1
fi

# tools search priority:
# 1) $HCC_HOME
# 2) @CMAKE_INSTALL_PREFIX@ : default install directory
# 3) @LLVM_TOOLS_DIR@ : build directory

if [ -n "$HCC_HOME" ] && [ -e "$HCC_HOME" ]; then
    EMBED=$HCC_HOME/bin/clamp-embed
    AS=$HCC_HOME/bin/llvm-as
    OPT=$HCC_HOME/bin/opt
    LLC=$HCC_HOME/bin/llc
    LINK=$HCC_HOME/bin/llvm-link
    MATHLIB=$HCC_HOME/lib
    LIB=$HCC_HOME/lib
    HSATOOLS=$HCC_HOME/bin/clamp-hsatools
elif [ -e @CMAKE_INSTALL_PREFIX@ ]; then
    AS=@CMAKE_INSTALL_PREFIX@/bin/llvm-as
    OPT=@CMAKE_INSTALL_PREFIX@/bin/opt
    LLC=@CMAKE_INSTALL_PREFIX@/bin/llc
    LINK=@CMAKE_INSTALL_PREFIX@/bin/llvm-link
    MATHLIB=@CMAKE_INSTALL_PREFIX@/lib
    LIB=@CMAKE_INSTALL_PREFIX@/lib
    HSATOOLS=@CMAKE_INSTALL_PREFIX@/bin/clamp-hsatools
elif [ -d @LLVM_TOOLS_DIR@ ]; then
    AS=@LLVM_TOOLS_DIR@/llvm-as
    OPT=@LLVM_TOOLS_DIR@/opt
    LLC=@LLVM_TOOLS_DIR@/llc
    LINK=@LLVM_TOOLS_DIR@/llvm-link
    MATHLIB=@CPPAMP_SOURCE_DIR@/lib
    LIB=@LLVM_LIBS_DIR@
    HSATOOLS=@PROJECT_BINARY_DIR@/lib/clamp-hsatools
else
    echo "ERROR: Can NOT locate HCC tools! Please specify with $HCC_HOME environ
mental variable." >&2
    exit 1
fi

################
# Verbose flag
################

VERBOSE=0

# set verbose flag
ARGS="$@"
for ARG in $ARGS
do
  if [ $ARG == "--verbose" ]; then
    VERBOSE=1
  fi
done

HSA_USE_AMDGPU_BACKEND=@HSA_USE_AMDGPU_BACKEND@

if [ $HSA_USE_AMDGPU_BACKEND == "ON" ]; then
  KM_USE_AMDGPU="${KM_USE_AMDGPU:=1}"
fi

# emit HSAIL/BRIG kernel
if [ "$3" == "--hsa" ]; then
    if [ $KMDUMPLLVM == "1" ]; then
      cp $1 ./dump.input.bc
    fi

    if [ "$CLAMP_NOTILECHECK" == "ON" ]; then
      if [ "$ALWAYS_MALLOC" == "ON" ]; then
        $OPT -load $LIB/LLVMPromote@CMAKE_SHARED_LIBRARY_SUFFIX@ \
             -load $LIB/LLVMEraseNonkernel@CMAKE_SHARED_LIBRARY_SUFFIX@ \
             -promote-globals -promote-privates -erase-nonkernels -malloc-select -alwasy-malloc -dce -globaldce -S < $1 -o $2.promote.ll.orig
      else
        $OPT -load $LIB/LLVMPromote@CMAKE_SHARED_LIBRARY_SUFFIX@ \
             -load $LIB/LLVMEraseNonkernel@CMAKE_SHARED_LIBRARY_SUFFIX@ \
             -promote-globals -promote-privates -erase-nonkernels -malloc-select -dce -globaldce -S < $1 -o $2.promote.ll.orig
      fi
    else
      if [ "$ALWAYS_MALLOC" == "ON" ]; then
        $OPT -load $LIB/LLVMPromote@CMAKE_SHARED_LIBRARY_SUFFIX@ \
             -load $LIB/LLVMEraseNonkernel@CMAKE_SHARED_LIBRARY_SUFFIX@ \
             -load $LIB/LLVMTileUniform@CMAKE_SHARED_LIBRARY_SUFFIX@ \
             -promote-globals -promote-privates -erase-nonkernels -tile-uniform -malloc-select -alwasy-malloc -dce -globaldce -S < $1 -o $2.promote.ll.orig
      else
        $OPT -load $LIB/LLVMPromote@CMAKE_SHARED_LIBRARY_SUFFIX@ \
             -load $LIB/LLVMEraseNonkernel@CMAKE_SHARED_LIBRARY_SUFFIX@ \
             -load $LIB/LLVMTileUniform@CMAKE_SHARED_LIBRARY_SUFFIX@ \
             -promote-globals -promote-privates -erase-nonkernels -tile-uniform -malloc-select -dce -globaldce -S < $1 -o $2.promote.ll.orig

      fi
    fi

    # remove special section information for AMDGPU backend
    if [ $KM_USE_AMDGPU ] ; then
      $OPT -load $LIB/LLVMRemoveSpecialSection@CMAKE_SHARED_LIBRARY_SUFFIX@ \
           -remove-special-section -S < $2.promote.ll.orig -o $2.promote.ll.orig.new
      if [ $? == 0 ]; then
        mv -f $2.promote.ll.orig.new $2.promote.ll.orig
      fi
    fi

    if [ $? == 1 ]; then
      echo "Generating HSAIL BRIG kernel failed"
      exit 1
    fi
    sed "s/call /call spir_func /g" < $2.promote.ll.orig | sed "s/addrspacecast /bitcast /g" > $2.promote.ll

    if [ $KMDUMPLLVM == "1" ]; then
      cp $2.promote.ll ./dump.promote.ll
    fi

    $AS -o $2.promote.bc $2.promote.ll

    if [ $VERBOSE == 1 ]; then
      echo "Generating HSA Brig kernel"
    fi
    $LINK $MATHLIB/hsa_math.bc $2.promote.bc -o $2 2>/dev/null

    if [ $KMDUMPLLVM == "1" ]; then
      cp $2 ./dump.hsa_math_linked.bc
    fi

    $HSATOOLS $2
    RETVAL=$?
    if [ $RETVAL == 0 ]; then
      mv -f $2 $2.orig
      mv $2.brig $2
      # remove temp file
      rm $2.promote.ll.orig $2.promote.ll $2.promote.bc
    fi
    exit $RETVAL
fi

# emit SPIR kernel
if [ "$3" == "--spir" ]; then
    if [ "$CLAMP_NOTILECHECK" == "ON" ]; then
      $OPT -load $LIB/LLVMPromote@CMAKE_SHARED_LIBRARY_SUFFIX@ \
           -load $LIB/LLVMEraseNonkernel@CMAKE_SHARED_LIBRARY_SUFFIX@ \
           -promote-globals -erase-nonkernels -dce -globaldce < $1 -o $2.promote.bc
    else
      $OPT -load $LIB/LLVMPromote@CMAKE_SHARED_LIBRARY_SUFFIX@ \
           -load $LIB/LLVMEraseNonkernel@CMAKE_SHARED_LIBRARY_SUFFIX@ \
           -load $LIB/LLVMTileUniform@CMAKE_SHARED_LIBRARY_SUFFIX@ \
           -promote-globals -erase-nonkernels -tile-uniform -dce -globaldce < $1 -o $2.promote.bc
    fi
    if [ $? == 1 ]; then
      echo "Generating OpenCL SPIR kernel failed"
      exit 1
    fi
    if [ $VERBOSE == 1 ]; then
      echo "Generating OpenCL SPIR kernel"
    fi
    $LINK $MATHLIB/opencl_math.bc $2.promote.bc -o $2 2>/dev/null
    # remove temp file
    rm $2.promote.bc
    exit $?
fi

# emit OpenCL C kernel
if [ "$3" == "--opencl" ]; then
    if [ "$CLAMP_NOTILECHECK" == "ON" ]; then
      $OPT -load $LIB/LLVMPromote@CMAKE_SHARED_LIBRARY_SUFFIX@ \
           -load $LIB/LLVMEraseNonkernel@CMAKE_SHARED_LIBRARY_SUFFIX@ \
           -promote-globals -erase-nonkernels -dce -globaldce < $1 -o $2.promote.bc
    else
      $OPT -load $LIB/LLVMPromote@CMAKE_SHARED_LIBRARY_SUFFIX@ \
           -load $LIB/LLVMEraseNonkernel@CMAKE_SHARED_LIBRARY_SUFFIX@ \
           -load $LIB/LLVMTileUniform@CMAKE_SHARED_LIBRARY_SUFFIX@ \
           -promote-globals -erase-nonkernels -tile-uniform -dce -globaldce < $1 -o $2.promote.bc
    fi
    if [ $? == 1 ]; then
      echo "Generating OpenCL C kernel failed"
      exit 1
    fi
    if [ $VERBOSE == 1 ]; then
      echo "Generating OpenCL C kernel"
    fi
    $LLC $2.promote.bc -march=c -o - |cat $MATHLIB/opencl_math.cl - > $2

    if grep -q " double " $2; then
        cat $MATHLIB/opencl_prefix.cl $2 > $2.t
        mv -f $2.t $2
    fi
    # remove temp file
    rm $2.promote.bc
    exit $?
fi


hasSPIR() {
    [ -z "$CLAMP_NOSPIR" -a -x /usr/bin/clinfo ] && ( /usr/bin/clinfo|grep cl_khr_spir > /dev/null )
    return $?
}
# if there is no mode specified, fallback to default behavior
if [ "$CLAMP_NOTILECHECK" == "ON" ]; then
  $OPT -load $LIB/LLVMPromote@CMAKE_SHARED_LIBRARY_SUFFIX@ \
       -load $LIB/LLVMEraseNonkernel@CMAKE_SHARED_LIBRARY_SUFFIX@ \
       -promote-globals -erase-nonkernels -dce -globaldce < $1 -o $2.promote.bc
else
  $OPT -load $LIB/LLVMPromote@CMAKE_SHARED_LIBRARY_SUFFIX@ \
       -load $LIB/LLVMEraseNonkernel@CMAKE_SHARED_LIBRARY_SUFFIX@ \
       -load $LIB/LLVMTileUniform@CMAKE_SHARED_LIBRARY_SUFFIX@ \
       -promote-globals -erase-nonkernels -tile-uniform -dce -globaldce < $1 -o $2.promote.bc
fi
if [ $? == 1 ]; then
  echo "Generating OpenCL kernel failed"
  exit 1
fi
if hasSPIR; then
    if [ $VERBOSE == 1 ]; then
      echo "Generating OpenCL SPIR kernel"
    fi
    $LINK $MATHLIB/opencl_math.bc $2.promote.bc -o $2 2>/dev/null
    # remove temp file
    rm $2.promote.bc
    exit $?
else
    if [ $VERBOSE == 1 ]; then
      echo "Generating OpenCL C kernel"
    fi
    $LLC $2.promote.bc -march=c -o - |cat $MATHLIB/opencl_math.cl - > $2

    if grep -q " double " $2; then
        cat $MATHLIB/opencl_prefix.cl $2 > $2.t
        mv -f $2.t $2
    fi
    # remove temp file
    rm $2.promote.bc
    exit $?
fi
